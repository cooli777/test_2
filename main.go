package main

import (
	"flag"
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

func main() {
	length := flag.Int("n", 42, "an int")
	flag.Parse()

	arr := generate2DArray(*length)
	showArray2D(arr)
	arrCollection := create1DArrayFrom2dArray(arr, *length)
	fmt.Printf("arrTo %+v\n", arrCollection.GetAll())
}

//showArray2D вывести двумерный массив на экран
func showArray2D(arr [][]int) {
	fmt.Printf("2d array: \n")
	for row := 0; row < len(arr); row++ {
		fmt.Printf("%+v\n", arr[row])
	}
}

//generate2DArray создаем двумерный массив
func generate2DArray(length int) [][]int {
	arrLength := getArrayLength(length)

	arr := make([][]int, arrLength, arrLength)

	for row := 0; row < arrLength; row++ {
		arr[row] = make([]int, arrLength, arrLength)
		for column := 0; column < arrLength; column++ {
			arr[row][column] = rand.Intn(100)
		}
	}

	return arr
}

//arrLength получаем кол-во элементов в строке  массива
func getArrayLength(length int) int {
	return length*2 - 1
}

//create1DArrayFrom2dArray переводим массив из 2-мерного в 1-мерны
//по спирали из центра массива до элемента [0][0]
func create1DArrayFrom2dArray(arr [][]int, length int) ArrCollection {
	centerRow := length - 1
	centerColumn := length - 1

	arrLength := getArrayLength(length)
	arrCollection := createArrCollection(arrLength)

	var column, row = 0, 0

	arrCollection.AddByIndex(0, arr[centerRow][centerColumn])

	for lap := length; lap > 0; lap-- {

		//right
		for ; column < arrLength-(length-lap)-1; column++ {
			arrCollection.Add(arr[row][column])
		}

		//down
		for ; row < arrLength-(length-lap)-1; row++ {
			arrCollection.Add(arr[row][column])
		}

		//left
		for ; column > (length - lap); column-- {
			arrCollection.Add(arr[row][column])
		}

		//top
		for ; row > (length - lap); row-- {
			arrCollection.Add(arr[row][column])
		}

		row++
		column++
	}

	return arrCollection
}

type ArrCollection struct {
	arr     []int
	counter int
}

//Add добавить элемент в массив
func (self *ArrCollection) Add(element int) {
	self.arr[self.counter] = element
	self.counter--
}

//Add добавить элемент в массив по индексу
func (self *ArrCollection) AddByIndex(index int, element int) {
	self.arr[index] = element
}

//GetAll  вернуть массив
func (self *ArrCollection) GetAll() []int {
	return self.arr
}

//createArrCollection создание коллекции
func createArrCollection(arrLength int) ArrCollection {
	countElements := arrLength * arrLength
	return ArrCollection{make([]int, countElements, countElements), countElements - 1}
}